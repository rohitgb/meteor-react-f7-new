import React from "react";
import Info from "./Info.jsx";
import routes from "./routes.js";
import Hello from "./Hello.jsx";

const f7params = {
  id: "io.framework7.testapp",
  name: "MyBy",
  theme: "auto",
  routes,

  statusbar: {
    iosBackgroundColor: "#ffffff",
    androidBackgroundColor: "#ffffff",
    transition: "400ms",
  },

  view: {
    iosDynamicNavbar: false,
    pushState: true,

    pushStateSeparator: "",
    routableModals: true,

    iosSwipeBack: false,
  },

  input: {
    scrollIntoViewOnFocus: true,
    scrollIntoViewAlways: true,
    scrollIntoViewCentered: true,
  },
  panel: {
    swipe: "both",
  },
  picker: {
    rotateEffect: true,
    openIn: "sheet",
  },
  autocomplete: {
    openIn: "dropdown",
    animate: false,
  },
};

const MainApp = () => (
  <App params={f7params}>
    <View>
      <Info />
    </View>
  </App>
);
export default MainApp;
