import Hello from "./Hello";

export default [
  {
    path: "/hello-page/",
    component: Hello,
  },
];
