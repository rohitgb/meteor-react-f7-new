import React, { Component } from "react";

class Info extends Component {
  render() {
    return (
      <Page pageContent={false}>
        <Navbar backLink="back">
          <NavLeft>App</NavLeft>
        </Navbar>
        <PageContent>
          <Block>
            <h1>Welcome</h1>
            <h2>Meteor + Framework7 + React</h2>

            <Link href="/hello-page/">Go to hello page</Link>
          </Block>
        </PageContent>
      </Page>
    );
  }
}

export default Info;
